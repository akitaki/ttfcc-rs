#version 410 core

layout (location = 0) in vec3 in_pos;
layout (location = 1) in vec2 in_face_pos;
layout (location = 2) in int in_texture_id;

out vec2 vert_texture_coord;

// Perspective projection matrix
uniform mat4 proj;
// Camera view matrix
uniform mat4 view;
// Chunk shift (p, r, q)
uniform vec3 shift;

void main() {
    gl_Position = vec4(1.0) + proj * view * (
        vec4(shift.x * 16.0, shift.y * 16.0, shift.z * 16.0, 0.0)
        + vec4(in_pos, 1.0)
    );

    float texture_coord_x = 0.25 * float(in_texture_id % 4) + 0.25 * in_face_pos.x;
    float texture_coord_y = 0.25 * float(in_texture_id / 4) + 0.25 * in_face_pos.y;
    vert_texture_coord = vec2(texture_coord_x, texture_coord_y);
}
