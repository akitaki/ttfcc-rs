#version 410 core

in vec2 vert_texture_coord;

out vec4 frag_color;

uniform sampler2D main_texture;

void main() {
    vec2 texture_size = textureSize(main_texture, 0);
    frag_color = texture(
        main_texture,
        vert_texture_coord
    );
}
