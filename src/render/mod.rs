use gl::types::*;
use std::ffi::{CStr, CString};
use std::ptr::{null, null_mut};

mod camera;
pub use camera::Camera;

/// Safe (or at least safer) wrapper for a GL shader program.
pub struct Shader {
    program: GLuint,
}

#[derive(Debug, Copy, Clone)]
pub struct ShaderHandle {
    program: GLuint,
}

impl Shader {
    pub fn new(
        vert_shader_source: &'static str,
        frag_shader_source: &'static str,
    ) -> Result<Self, Error> {
        // Compile and link source as program
        let program = unsafe {
            // Compile and check errors for each shader
            let vert_shader =
                Self::compile_shader(vert_shader_source, gl::VERTEX_SHADER)?;
            let frag_shader =
                Self::compile_shader(frag_shader_source, gl::FRAGMENT_SHADER)?;

            // Link and check errors, then return to `self.program`
            Self::link_shader(vert_shader, frag_shader)?
        };

        Ok(Self { program })
    }

    pub fn bind<F, T>(&self, callback: F) -> T
    where
        F: FnOnce(ShaderHandle) -> T,
    {
        unsafe { gl::UseProgram(self.program) }
        let output: T = callback(ShaderHandle {
            program: self.program,
        });
        #[cfg(debug_assertions)]
        unsafe {
            gl::UseProgram(0)
        }
        output
    }

    unsafe fn compile_shader(
        source: &'static str,
        type_: GLenum,
    ) -> Result<GLuint, Error> {
        let shader = gl::CreateShader(type_);
        let c_source: CString =
            CString::new(source).expect("Creation of CString failed");

        gl::ShaderSource(
            shader,             // shader
            1 as GLsizei,       // count
            &c_source.as_ptr(), // string
            null(),             // length; null-terminated
        );
        gl::CompileShader(shader);

        Self::check_compile_error(shader)?;

        Ok(shader)
    }

    /// Return the linked shader program
    unsafe fn link_shader(
        vert_shader: GLuint,
        frag_shader: GLuint,
    ) -> Result<GLuint, Error> {
        let program = gl::CreateProgram();
        gl::AttachShader(program, vert_shader);
        gl::AttachShader(program, frag_shader);
        gl::LinkProgram(program);

        Self::check_link_error(program)?;

        Ok(program)
    }

    unsafe fn check_compile_error(shader: GLuint) -> Result<(), Error> {
        let mut success = gl::FALSE as GLint;
        gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut success);

        if success != gl::TRUE as GLint {
            let mut info_log = Vec::with_capacity(512);
            info_log.set_len(512 - 1);
            gl::GetShaderInfoLog(
                shader,
                512,
                null_mut(),
                info_log.as_mut_ptr(),
            );
            let info_log = CStr::from_ptr(info_log.as_ptr());

            Err(Error::ShaderCompile(info_log.to_string_lossy().into()))
        } else {
            Ok(())
        }
    }

    unsafe fn check_link_error(program: GLuint) -> Result<(), Error> {
        let mut success = gl::FALSE as GLint;
        let mut info_log = Vec::with_capacity(512);
        info_log.set_len(512 - 1);
        gl::GetProgramiv(program, gl::LINK_STATUS, &mut success);

        if success != gl::TRUE as GLint {
            gl::GetProgramInfoLog(
                program,
                512,
                null_mut(),
                info_log.as_mut_ptr(),
            );
            let info_log = CStr::from_ptr(info_log.as_ptr());

            Err(Error::ShaderLink(info_log.to_string_lossy().into()))
        } else {
            Ok(())
        }
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.program);
        }
    }
}

impl ShaderHandle {
    pub fn uniform_vec3(
        &self,
        name: &str,
        data: glm::Vec3,
    ) -> Result<(), Error> {
        unsafe {
            let c_name =
                CString::new(name).expect("Failed to create name CString");
            let location =
                gl::GetUniformLocation(self.program, c_name.as_ptr());
            if location == -1 {
                return Err(Error::UniformLocation(format!("{}", name)));
            }
            gl::Uniform3f(location, data[0], data[1], data[2]);
        }
        Ok(())
    }

    pub fn uniform_mat4(
        &self,
        name: &str,
        data: glm::Mat4,
    ) -> Result<(), Error> {
        unsafe {
            let c_name =
                CString::new(name).expect("Failed to create name CString");
            let location =
                gl::GetUniformLocation(self.program, c_name.as_ptr());
            if location == -1 {
                return Err(Error::UniformLocation(format!("{}", name)));
            }
            gl::UniformMatrix4fv(location, 1, 0, data.as_ptr());
        }
        Ok(())
    }

    pub fn set_depth_test_enabled(&self, enabled: bool) {
        unsafe {
            if enabled {
                gl::Enable(gl::DEPTH_TEST);
            } else {
                gl::Disable(gl::DEPTH_TEST);
            }
        }
    }

    pub fn set_cull_face_enabled(&self, enabled: bool) {
        unsafe {
            if enabled {
                gl::Enable(gl::CULL_FACE);
            } else {
                gl::Disable(gl::CULL_FACE);
            }
        }
    }
}

pub struct Texture {
    texture: GLuint,
}

impl Texture {
    pub fn new(_: ShaderHandle, image: image::DynamicImage) -> Self {
        use image::GenericImageView;
        use std::ffi::c_void;

        let image_bytes = image.flipv().to_bytes();
        let mut texture = 0;
        unsafe {
            gl::GenTextures(1, &mut texture);
            gl::BindTexture(gl::TEXTURE_2D, texture);
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                gl::RGBA as GLint,
                image.width() as GLsizei,
                image.height() as GLsizei,
                0,
                gl::RGBA,
                gl::UNSIGNED_BYTE,
                image_bytes.as_ptr() as *const c_void,
            );
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_MIN_FILTER,
                gl::NEAREST as GLint,
            );
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_MAG_FILTER,
                gl::NEAREST as GLint,
            );
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_WRAP_S,
                gl::CLAMP_TO_EDGE as GLint,
            );
            gl::TexParameteri(
                gl::TEXTURE_2D,
                gl::TEXTURE_WRAP_T,
                gl::CLAMP_TO_EDGE as GLint,
            );
            gl::GenerateMipmap(gl::TEXTURE_2D);
        }
        Self { texture }
    }

    pub fn bind(&self) {
        unsafe { gl::BindTexture(gl::TEXTURE_2D, self.texture) }
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe { gl::DeleteTextures(1, &self.texture) }
    }
}

#[derive(Debug)]
enum AttribPtr {
    Float(AttribPtrData),
    Int(AttribPtrData),
}

#[derive(Debug)]
struct AttribPtrData {
    size: GLint,
    type_: GLenum,
    stride: GLsizei,
    offset: usize,
}

impl AttribPtr {
    pub fn new_float(size: GLint, type_: GLenum, offset: usize) -> Self {
        Self::Float(AttribPtrData {
            size,
            type_,
            offset,
            stride: 0,
        })
    }

    pub fn new_int(size: GLint, type_: GLenum, offset: usize) -> Self {
        Self::Int(AttribPtrData {
            size,
            type_,
            offset,
            stride: 0,
        })
    }

    fn get_inner(&mut self) -> &mut AttribPtrData {
        match self {
            Self::Float(inner) | Self::Int(inner) => inner,
        }
    }

    pub fn set_stride(&mut self, stride: GLsizei) {
        self.get_inner().stride = stride;
    }

    pub fn run(&self, index: GLuint) {
        match self {
            Self::Float(inner) => unsafe {
                gl::VertexAttribPointer(
                    index,                         // index
                    inner.size,                    // size
                    inner.type_,                   // type
                    gl::FALSE,                     // normalized
                    inner.stride,                  // stride
                    inner.offset as *const GLvoid, // pointer
                );
                gl::EnableVertexAttribArray(index);
            },
            Self::Int(inner) => unsafe {
                gl::VertexAttribIPointer(
                    index,                         // index
                    inner.size,                    // size
                    inner.type_,                   // type
                    inner.stride,                  // stride
                    inner.offset as *const GLvoid, // pointer
                );
                gl::EnableVertexAttribArray(index);
            },
        }
    }
}

#[derive(Debug)]
pub struct AttribPtrList {
    list: Vec<AttribPtr>,
    acc_size: usize,
}

impl AttribPtrList {
    pub fn new() -> Self {
        Self {
            list: vec![],
            acc_size: 0,
        }
    }

    pub fn push_float(mut self, size: i8, type_: GLenum) -> Self {
        let new_attrib =
            AttribPtr::new_float(size as i32, type_, self.acc_size);
        self.list.push(new_attrib);

        let type_size = Self::size_of_gl_type(type_);
        self.acc_size += type_size * size as usize;

        self
    }

    pub fn push_int(mut self, size: i8, type_: GLenum) -> Self {
        let new_attrib = AttribPtr::new_int(size as i32, type_, self.acc_size);
        self.list.push(new_attrib);

        let type_size = Self::size_of_gl_type(type_);
        self.acc_size += type_size * size as usize;

        self
    }

    pub fn run(mut self, _: DrawBufferHandle) {
        self.update_strides();
        for (index, attrib) in self.list.iter().enumerate() {
            attrib.run(index as GLuint);
        }
    }

    fn update_strides(&mut self) {
        for attrib in self.list.iter_mut() {
            attrib.set_stride(self.acc_size as GLsizei);
        }
    }

    #[rustfmt::skip]
    fn size_of_gl_type(type_: GLenum) -> usize {
        use gl::*;
        use std::mem::size_of;

        match type_ {
            BYTE           => size_of::<GLbyte>(),
            UNSIGNED_BYTE  => size_of::<GLubyte>(),
            SHORT          => size_of::<GLshort>(),
            UNSIGNED_SHORT => size_of::<GLushort>(),
            INT            => size_of::<GLint>(),
            UNSIGNED_INT   => size_of::<GLuint>(),
            FLOAT          => size_of::<GLfloat>(),
            DOUBLE         => size_of::<GLdouble>(),
            _              => panic!("Unknown type at size_of_gl_type"),
        }
    }
}

pub struct RenderBuffer<V> {
    vertices: Vec<V>,
    indices: Vec<u32>,
}

pub type BlockRenderBuffer = RenderBuffer<BlockVertex>;

impl<V> RenderBuffer<V> {
    pub fn new() -> Self {
        Self {
            vertices: vec![],
            indices: vec![],
        }
    }

    pub fn push_vertice(&mut self, vertice: V) {
        self.vertices.push(vertice);
    }

    pub fn push_indice(&mut self, indice: u32) {
        self.indices.push(indice);
    }

    pub fn get_last_indice(&self) -> Option<u32> {
        self.indices.last().copied()
    }

    pub fn is_empty(&self) -> bool {
        self.vertices.is_empty()
    }
}

#[repr(C, packed)]
pub struct BlockVertex {
    pub pos: [f32; 3],
    pub face_pos: [f32; 2],
    pub texture_id: u8,
}

/// A safer wrapper for VAO, VBO and EBO
#[derive(Debug)]
pub struct DrawBuffer {
    vao: GLuint,
    vbo: GLuint,
    ebo: GLuint,
    finished: bool,
    indice_len: usize,
}

#[derive(Debug)]
pub struct DrawBufferHandle<'a> {
    inner: &'a mut DrawBuffer,
}

impl DrawBuffer {
    /// Create new draw buffer and set it up with the provided `attrib_ptr_list`.
    /// Requires a shader to be bound.
    pub fn new(handle: ShaderHandle, attrib_ptr_list: AttribPtrList) -> Self {
        let [vao, vbo, ebo] = Self::gen_objs();
        let mut draw_buffer = Self {
            vao,
            vbo,
            ebo,
            finished: false,
            indice_len: 0,
        };
        draw_buffer.bind(handle, |handle| {
            attrib_ptr_list.run(handle);
        });
        draw_buffer
    }

    pub fn bind<F>(&mut self, _: ShaderHandle, callback: F)
    where
        F: FnOnce(DrawBufferHandle),
    {
        // Bind the buffers
        unsafe {
            gl::BindVertexArray(self.vao);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.ebo);
        }

        // Run the closure
        callback(DrawBufferHandle { inner: self });

        // Unbind the buffers
        #[cfg(debug_assertions)]
        unsafe {
            gl::BindVertexArray(0);
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }
    }

    /// Generate [vao, vbo, ebo] array
    fn gen_objs() -> [GLuint; 3] {
        let (mut vao, mut vbo, mut ebo) = (0, 0, 0);

        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::GenBuffers(1, &mut vbo);
            gl::GenBuffers(1, &mut ebo);
        }

        [vao, vbo, ebo]
    }
}

impl Drop for DrawBuffer {
    /// Delete underlying GL objects
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &mut self.vao);
            gl::DeleteBuffers(1, &mut self.vbo);
            gl::DeleteBuffers(1, &mut self.ebo);
        }
    }
}

impl<'a> DrawBufferHandle<'a> {
    pub fn feed_render_buffer<V>(&mut self, render_buffer: &RenderBuffer<V>) {
        self.feed_indices_data(&render_buffer.indices);
        self.feed_vertices_data(&render_buffer.vertices);
    }

    pub fn draw_triangles(&self, type_: GLenum) {
        unsafe {
            gl::DrawElements(
                gl::TRIANGLES,
                self.inner.indice_len as GLsizei,
                type_,
                0 as *const GLvoid,
            );
        }
    }

    fn feed_vertices_data<T>(&mut self, vertices: &[T]) {
        unsafe {
            gl::BufferData(
                gl::ARRAY_BUFFER,
                std::mem::size_of_val(vertices) as GLsizeiptr,
                vertices.as_ptr() as *const GLvoid,
                gl::DYNAMIC_DRAW,
            );
        }
    }

    fn feed_indices_data<T>(&mut self, indices: &[T]) {
        unsafe {
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                std::mem::size_of_val(indices) as GLsizeiptr,
                indices.as_ptr() as *const GLvoid,
                gl::DYNAMIC_DRAW,
            );
        }
        self.inner.indice_len = indices.len();
    }
}

#[derive(Debug, Clone)]
pub enum Error {
    ShaderCompile(String),
    ShaderLink(String),
    UniformLocation(String),
}

pub fn clear_buffer() {
    unsafe {
        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        gl::ClearColor(0.5, 0.5, 0.5, 1.0);
    }
}

pub fn get_proj_matrix((width, height): (u32, u32)) -> glm::Mat4 {
    glm::perspective_fov(
        glm::pi::<f32>() * 45.0 / 180.0,
        width as f32,
        height as f32,
        0.05,
        1000.0,
    )
}

pub fn set_proj_and_view_uniform(
    handle: ShaderHandle,
    (width, height): (u32, u32),
    view: glm::Mat4,
) {
    handle
        .uniform_mat4("view", view)
        .expect("Failed to set view matrix uniform");
    handle
        .uniform_mat4(
            "proj",
            glm::perspective_fov(
                glm::pi::<f32>() * 45.0 / 180.0,
                width as f32,
                height as f32,
                0.05,
                1000.0,
            ),
        )
        .expect("Failed to set proj matrix uniform");
}

#[allow(unused_variables)]
extern "system" fn gl_error_callback(
    source: GLenum,
    gltype: GLenum,
    id: GLuint,
    severity: GLenum,
    length: GLsizei,
    message: *const GLchar,
    user_param: *mut std::ffi::c_void,
) {
    match severity {
        gl::DEBUG_SEVERITY_MEDIUM | gl::DEBUG_SEVERITY_HIGH => {
            let c_message = unsafe { std::ffi::CStr::from_ptr(message) };
            let rs_message = c_message.to_string_lossy();
            println!(
                "OpenGL error (source: {}, gltype: {}): {}",
                source, gltype, rs_message
            );
        }
        _ => {}
    }
}

pub unsafe fn setup_gl_error_callback() {
    gl::DebugMessageCallback(Some(gl_error_callback), std::ptr::null());
}
