use super::{Block, Chunk, ChunkPos, LocalBlockPos};

pub struct ChunkPopulator {
    seed: i32,
}

impl ChunkPopulator {
    pub fn new(seed: i32) -> Self {
        Self { seed }
    }

    /// Populate the chunk with blocks
    pub fn populate(&self, chunk: &mut Chunk) {
        let pos = chunk.get_pos();

        let height_map = self.get_height_map(pos);

        for (x, z) in iproduct!(0..16, 0..16) {
            for y in 0..height_map[(x as usize) + (z as usize) * 16] {
                chunk.set_block(LocalBlockPos::new(x, y, z), Block::Stone);
            }
        }
    }

    fn get_height_map(&self, pos: ChunkPos) -> Vec<u8> {
        // The original heightmap with values in [-1, 1]
        let (height_map, _min, _max) = simdnoise::NoiseBuilder::fbm_2d_offset(
            (pos.p * 16) as f32, // x-offset
            16,                  // width
            (pos.q * 16) as f32, // y-offset
            16,                  // height
        )
        .with_seed(self.seed)
        .generate();

        // Remap to height values
        height_map
            .into_iter()
            .map(|f| ((f + 1.0) * 140.0).round() as u8)
            .collect()
    }
}
